package main

import (
	"runtime"

	"./app"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	var app = app.App{}
	app.Init()
	//app.InitSheduler()
}
