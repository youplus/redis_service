package tasks

import (
	"encoding/json"
	"errors"
	"log"
	"strconv"
	"strings"

	"../infrastructure"
	"../models"
	"github.com/gomodule/redigo/redis"
	"github.com/mitchellh/mapstructure"
	"github.com/olivere/elastic"
)

//PushService ...
type PushService struct{}

var redisAccess = infrastructure.RedisAccess{}
var redisPool = redisAccess.GetRedis() //Accessing redis pool
var es = infrastructure.ElasticSearch{}
var config = infrastructure.Configuration{}

//PopulateWidgetVideos ...
func (ps *PushService) PopulateWidgetVideos(widgetConfig map[string]interface{}, filterTags map[string][]string) error {
	log.Println("In PopulateWidgetVideos function")
	var err error
	var reWidgetConfig models.RedisWidgetConfig
	var res []map[string]interface{}
	mapstructureConfig := &mapstructure.DecoderConfig{TagName: "json"}
	for ok := true; ok; ok = true {
		mapstructureConfig.Result = &reWidgetConfig
		decoder, _ := mapstructure.NewDecoder(mapstructureConfig)
		decoder.Decode(widgetConfig)

		var redisSts interface{}
		conn := redisPool.Get() // Get a redis connection
		defer conn.Close()
		conn.Send("MULTI") //Initializing bulk/ multi operation.

		log.Println("video_orders IDs:")
		log.Println(widgetConfig["video_orders"])
		var qTags models.RedisQTags
		vidOrders := widgetConfig["video_orders"].([]interface{})
		reZsetScore := -1
		widgetID := widgetConfig["_id"].(string)

		if widgetConfig["video_orders"] != nil && len(vidOrders) > 0 {
			reWidgetConfig.Ordered = true
			res, err = es.GetOderedVid(widgetConfig["video_orders"], config.QVMIndex)
			if err != nil {
				break
			}
			log.Println("Number of orderd videos: " + string(len(res)))
			for _, vid := range res {
				if vid["found"] == true {
					mapstructureConfig.Result = &qTags
					decoder, _ := mapstructure.NewDecoder(mapstructureConfig)
					decoder.Decode(vid["_source"])
					qTags.ID = vid["_id"].(string)
					reZsetScore++
					log.Println(reZsetScore)
					log.Println("Score:" + string(reZsetScore) + ". Doing HMSET and ZADD for " + qTags.ID)
					conn.Send("HMSET", redis.Args{}.Add("qt_"+qTags.ID).AddFlat(&qTags)...) //mluti insertion
					conn.Send("ZADD", "wi_"+widgetID, reZsetScore, "qt_"+qTags.ID)
				}

			}

		}

		//constructing filter tags for ES query.  This is to excludes the videos which containes tags(bad strings) from the ES response
		var filterArray []string
		for _, tag := range filterTags["filter_tags"] {
			filterArray = append(filterArray, `"`+tag+`"`)
		}
		filterArrayStr := strings.Join(filterArray, ",")
		//constructing video_orders array for ES query. This is to excludes the array elements(vid ids) from the ES response
		var ordrdVidArray []string
		for _, id := range vidOrders {
			ordrdVidArray = append(ordrdVidArray, `"`+id.(string)+`"`)
		}
		ordrdVidArrayStr := strings.Join(ordrdVidArray, ",")
		//Getting videos for cbm by excluding "video_orders" array.
		var esRes *elastic.SearchHits
		body := []byte(`{ "size":` + strconv.Itoa(30-len(vidOrders)) + `,"query": { "bool": { "must": [ { "match": { "qs_c": { "operator": "and", "query": "` + widgetConfig["c"].(string) + `" } } }, { "match": { "qs_b": { "operator": "and", "query":"` + widgetConfig["b"].(string) + `"} } }, { "match": { "qs_m": { "operator": "and", "query":"` + widgetConfig["m"].(string) + `" } } }, { "match": { "qs_is_selected": { "operator": "and", "query": true } } } ], "must_not": [ { "terms": { "transcribed_text": [` + filterArrayStr + `] } }, { "terms": { "questions": [` + filterArrayStr + `] } }, { "terms": { "q_tags": [` + filterArrayStr + `] } }, { "ids" : { "values" : [` + ordrdVidArrayStr + `] } } ] } } }`)
		esRes, err = es.GetData(body, config.QVMIndex)
		if err != nil {
			break
		}

		log.Println("Number of Unorderd videos: " + string(esRes.TotalHits))
		byt, _ := json.Marshal(esRes.Hits)
		err = json.Unmarshal(byt, &res)
		for _, vid := range res {
			mapstructureConfig.Result = &qTags
			decoder, _ := mapstructure.NewDecoder(mapstructureConfig)
			decoder.Decode(vid["_source"])
			qTags.ID = vid["_id"].(string)
			reZsetScore++
			log.Println(reZsetScore)
			log.Println("Doing HMSET and ZADD for " + qTags.ID)
			conn.Send("HMSET", redis.Args{}.Add("qt_"+vid["_id"].(string)).AddFlat(&qTags)...) //mluti insertion
			conn.Send("ZADD", "wi_"+widgetID, reZsetScore, "qt_"+qTags.ID)

		}

		reWidgetConfig.Count = reZsetScore + 1
		var bytes []byte
		bytes, err = json.Marshal(&reWidgetConfig)
		if reZsetScore == -1 {
			conn.Close() //Closing Redis connection
			err = errors.New("Not enough videos")
			break
		}
		log.Println("Doing SET " + "wc_" + reWidgetConfig.WidgetURL + " " + string(bytes))
		conn.Send("SET", "wc_"+reWidgetConfig.WidgetURL, string(bytes)) //Writing widget_config to redis.
		redisSts, err = conn.Do("EXEC")                                 //Executing bulk/ multi operation.
		log.Println("Redis bulk excecuted status:")
		log.Println(redisSts)

		break //Break the loop
	}
	return err
}
