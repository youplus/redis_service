package controllers

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"../infrastructure"
	"../models"
	"../tasks"
	"github.com/olivere/elastic"
)

//Widget handles all the widget related events
type Widget struct{}

var config = infrastructure.Configuration{}
var wc = models.WidgetConfig{}
var redisAccess = infrastructure.RedisAccess{}
var es = infrastructure.ElasticSearch{}
var ps = tasks.PushService{}

//PushToRedis ...
func (wid *Widget) PushToRedis(widgetURL string) (bool, string, int) {
	success := true
	msg := "Successfully created"
	statusCode := http.StatusOK
	var err error
	var esRes *elastic.SearchHits
	widgetConfig := make(map[string]interface{})
	var filterTags map[string][]string

	//body := []byte(`{"query":{"bool":{"must":[{"nested":{"inner_hits":{"_source":["sites.widget_config"]},"query":{"bool":{"must":[{"match":{"sites.widget_config.widget_url":{"query":"` + widgetURL + `","operator":"and"}}}]}},"path":"sites.widget_config"}}]}}}`)
	body := []byte(`{"query":{"bool":{"must":[{"nested":{"inner_hits":{"_source":["sites.widget_config"]},"query":{"bool":{"must":[{"match":{"sites.widget_config.widget_url":{"query":"` + widgetURL + `","operator":"and"}}}]}},"path":"sites.widget_config"}},{"nested":{"inner_hits":{"_source":["sites.filter_tags"]},"query":{"bool":{"must":[{"match_all":{}}]}},"path":"sites"}}]}}}`)
	esRes, err = es.GetData(body, config.MongoUsersIndex)
	for ok := true; ok; ok = (err == nil) {
		if esRes.TotalHits == 0 {
			log.Println("URL is not configured yet")
			err = errors.New("URL is not configured yet")
			break
		}
		err = json.Unmarshal(*esRes.Hits[0].InnerHits["sites.widget_config"].Hits.Hits[0].Source, &widgetConfig)
		err = json.Unmarshal(*esRes.Hits[0].InnerHits["sites"].Hits.Hits[0].Source, &filterTags)
		//widgetConfig, filterTags, err = wc.GetWidgetConfig(widgetURL)

		if err == nil {
			err = ps.PopulateWidgetVideos(widgetConfig, filterTags)
		}
		break
	}

	if err != nil {
		success = false
		msg = "Failed to process: " + err.Error()
		statusCode = http.StatusBadRequest

	}
	return success, msg, statusCode
}

// pingRes := redis.PingRedis()
// log.Println(pingRes)
