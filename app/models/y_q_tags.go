package models

import "time"

//RedisQTags ...
type RedisQTags struct {
	ID                 string    `redis:"_id" json:"_id"`
	QsIsSelected       bool      `redis:"qs_is_selected" json:"qs_is_selected"`
	SourceImageURL     string    `redis:"source_image_url" json:"source_image_url"`
	QsI                string    `redis:"qs_i" json:"qs_i"`
	IsWidget           bool      `redis:"is_widget" json:"is_widget"`
	IsView             bool      `redis:"is_view" json:"is_view"`
	IsParentVideo      bool      `redis:"is_parent_video" json:"is_parent_video"`
	UserEmail          string    `redis:"user_email" json:"user_email"`
	Type               string    `redis:"type" json:"type"`
	QsC                string    `redis:"qs_c" json:"qs_c"`
	SearchTerm         string    `redis:"search_term" json:"search_term"`
	QsB                string    `redis:"qs_b" json:"qs_b"`
	QsID               string    `redis:"qs_id" json:"qs_id"`
	QsIsCorrected      bool      `redis:"qs_is_corrected" json:"qs_is_corrected"`
	Title              string    `redis:"title" json:"title"`
	IsReported         bool      `redis:"is_reported" json:"is_reported"`
	VideoURL           string    `redis:"video_url" json:"video_url"`
	Description        string    `redis:"description" json:"description"`
	UploadDate         time.Time `redis:"upload_date" json:"upload_date"`
	IsApproved         bool      `redis:"is_approved" json:"is_approved"`
	Taxonomy           []string  `redis:"taxonomy" json:"taxonomy"`
	QsM                string    `redis:"qs_m" json:"qs_m"`
	Question           string    `redis:"question" json:"question"`
	IndustryCode       int       `redis:"industry_code" json:"industry_code"`
	RatingByUser       string    `redis:"rating_by_user" json:"rating_by_user"`
	UserProfilePicture string    `redis:"user_profile_picture" json:"user_profile_picture"`
	SourceLink         string    `redis:"source_link" json:"source_link"`
	TranscribedText    string    `redis:"transcribed_text" json:"transcribed_text"`
	ParentVideoID      string    `redis:"parent_video_id" json:"parent_video_id"`
	QsMaxFlatScore     float64   `redis:"qs_max_flat_score" json:"qs_max_flat_score"`
	IndustryCategory   string    `redis:"industry_category" json:"industry_category"`
	SourceCountry      string    `redis:"source_country" json:"source_country"`
	VideoLanguage      string    `redis:"video_language" json:"video_language"`
	ManualSentiment    int       `redis:"manual_sentiment" json:"manual_sentiment"`
	Questions          []string  `redis:"questions" json:"questions"`
	QTagsID            string    `redis:"q_tags_id" json:"q_tags_id"`
	UserName           string    `redis:"user_name" json:"user_name"`
	Duration           float64   `redis:"duration" json:"duration"`
	UpdatedAt          time.Time `redis:"updated_at" json:"updated_at"`
	Source             string    `redis:"source" json:"source"`
	ThumbnailImageURL  string    `redis:"thumbnail_image_url" json:"thumbnail_image_url"`
	IsPrivate          bool      `redis:"is_private" json:"is_private"`
	ViewCount          int       `redis:"view_count" json:"view_count"`
}
