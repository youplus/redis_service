package models

//WidgetConfig is the model file which represents mongo_users.sites.widget_config collection in MongoDB
type WidgetConfig struct{}

//RedisWidgetConfig ...
type RedisWidgetConfig struct {
	WidgetID   string  `json:"_id" json:"_id"`
	WidgetURL  string  `json:"widget_url" json:"widget_url"`
	Count      int     `json:"count" json:"video_count"` //total count of videos
	ViewType   float64 `json:"view_type" json:"view_type"`
	Ordered    bool    `json:"ordered" json:"ordered"` //true if "video_orders" != empty
	Taxonomy   string  `json:"taxonomy" json:"taxonomy"`
	ViewStatus string  `json:"view_status" json:"view_status"`
}

// //GetWidgetConfig will retrun widget_config(mongo_users.sites.widget_config) object.
// func (wc *WidgetConfig) GetWidgetConfig(WidgetURL string) (map[string]interface{}, map[string][]string, error) {
// 	//success := true
// 	var esRes *elastic.SearchHits
// 	var widgetConfig map[string]interface{}
// 	var filterTags map[string][]string
// 	var err error
// 	//body := []byte(`{"query":{"bool":{"must":[{"nested":{"inner_hits":{"_source":["sites.widget_config"]},"query":{"bool":{"must":[{"match":{"sites.widget_config.widget_url":{"query":"` + WidgetURL + `","operator":"and"}}}]}},"path":"sites.widget_config"}}]}}}`)
// 	body := []byte(`{"query":{"bool":{"must":[{"nested":{"inner_hits":{"_source":["sites.widget_config"]},"query":{"bool":{"must":[{"match":{"sites.widget_config.widget_url":{"query":"` + WidgetURL + `","operator":"and"}}}]}},"path":"sites.widget_config"}},{"nested":{"inner_hits":{"_source":["sites.filter_tags"]},"query":{"bool":{"must":[{"match_all":{}}]}},"path":"sites"}}]}}}`)
// 	esRes, err = es.GetData(body, "mongo_users")

// 	for ok := true; ok; ok = (err == nil) {
// 		fmt.Println("===))))")
// 		fmt.Println(esRes.TotalHits)
// 		if esRes.TotalHits == 0 {
// 			err = errors.New("URL is not configured yet")
// 			break
// 		}
// 		err = json.Unmarshal(*esRes.Hits[0].InnerHits["sites.widget_config"].Hits.Hits[0].Source, &widgetConfig)
// 		err = json.Unmarshal(*esRes.Hits[0].InnerHits["sites"].Hits.Hits[0].Source, &filterTags)
// 		break
// 	}
// 	return widgetConfig, filterTags, err
// }
