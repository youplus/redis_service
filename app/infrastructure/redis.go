package infrastructure

import (
	"log"

	"github.com/gomodule/redigo/redis"
)

//RedisAccess ...
type RedisAccess struct{}

var conf = new(Configuration)
var config = conf.GetConfiguration()

//GetRedis : Establish connections to the Redis database
//Returns pool of connections.
func (rds *RedisAccess) GetRedis() *redis.Pool {
	// //Localhost
	// redisHost := "127.0.0.1"
	// redisPort := "6379"
	// password := ""
	//Server
	log.Println("Connecting to Redis...")
	redisHost := config.RedisHost
	redisPort := config.RedisPort
	password := config.RedisPassword
	// Establish a redis connection pool.
	pool := &redis.Pool{
		MaxIdle:   80,
		MaxActive: 1000, // max number of connections
		Dial: func() (redis.Conn, error) {
			//return redis.Dial("tcp", redisHost+":"+redisPort)
			c, err := redis.Dial("tcp", redisHost+":"+redisPort)
			if err != nil {
				return nil, err
			}
			if password != "" {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, err
		},
	}
	log.Println("Connected to Redis...")
	return pool
}

//PingRedis ...
func (rds *RedisAccess) PingRedis() interface{} {
	conn := rds.GetRedis().Get() //Getting Redis connection
	defer conn.Close()           //Closes the connection once task is done
	// Test the connection
	pongRes, err := conn.Do("PING")
	if err != nil {
		log.Fatal("Can't connect to the Redis database")
	}
	return pongRes
}
