package infrastructure

import (
	"log"

	"github.com/tkanos/gonfig"
)

//Configuration : Database configuration
type Configuration struct {
	MongoDBHost      string `json:"mongodbhost"`
	MongoDBDatabase  string `json:"mongodbdatabase"`
	MongoDBUsername  string `json:"mongodbusername"`
	MongoDBPassword  string `json:"mongodbpassword"`
	YouplusQTagsColl string `json:"youplusqtagscoll"`
	ElasticHost      string `json:"ElasticHost"`
	MongoUsersIndex  string `json:"MongoUsersIndex"`
	QVMIndex         string `json:"QVMIndex"`
	RedisHost        string `json:"RedisHost"`
	RedisPort        string `json:"RedisPort"`
	RedisPassword    string `json:"RedisPassword"`
}

//GetConfiguration : Returns configuration for the database
func (cfg *Configuration) GetConfiguration() Configuration {
	var configuration = Configuration{}
	var err = gonfig.GetConf("config.json", &configuration)
	if err != nil {
		log.Println("Error access configuration file")
	}
	return configuration
}
