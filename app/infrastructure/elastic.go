package infrastructure

import (
	"context"
	"encoding/json"
	"log"
	"sync"

	elastic "github.com/olivere/elastic"
)

var _initElasticCtx sync.Once
var _esInstance *ES
var _ctx = context.Background()
var db = DB{}

var _dbConfig = new(Configuration)

//ES ...
type ES struct {
	ElasticClient *elastic.Client
}

//ElasticSearch ...
type ElasticSearch struct{}

//GetElastic ...
func (es *ElasticSearch) GetElastic() *elastic.Client {
	_initElasticCtx.Do(func() {
		log.Println("Connecting to Elastic Search...")
		_config := _dbConfig.GetConfiguration()
		_client, err := elastic.NewSimpleClient(elastic.SetURL(_config.ElasticHost))
		_esInstance = &ES{ElasticClient: _client}
		if err != nil {
			log.Println("Error connecting to elastic search: " + err.Error())
		}
		log.Println("Connected to Elastic Search...")
	})
	return _esInstance.ElasticClient
}

//GetData ...
//Arguments: "body" => ES query. "indexName" => Name of the index were query to execute, Ex. "mongo_users"
//Return: 1.success=>true/false
//		  2.Returns result.Hits of ES response. Ex. returns "hits":{"total": 1,"max_score": 8.982418,"hits": []} in the below ES response.
// {  "took": 3,
//     "timed_out": false,
//     "_shards": {},
//     "hits":{
//				"total": 1,
//         		"max_score": 8.982418,
//         		"hits": []
//     		  }
// }
func (es *ElasticSearch) GetData(body []byte, indexName string) (*elastic.SearchHits, error) {
	//success := false
	var err error
	var esRes *elastic.SearchHits
	client := es.GetElastic()
	if client != nil {
		log.Println("Calling ES " + indexName + " Index with the query " + string(body))
		searchResponse, err := client.Search().
			Index(indexName).
			Source(string(body)).
			FetchSource(true).
			Pretty(true).
			Do(_ctx)
		if err == nil {
			esRes = searchResponse.Hits
		}
	}
	return esRes, err
}

//GetOderedVid ...
func (es *ElasticSearch) GetOderedVid(ids interface{}, indexName string) ([]map[string]interface{}, error) {
	var err error
	var data []map[string]interface{}
	client := es.GetElastic()
	if client != nil {
		log.Println("Calling ES " + indexName + " Index with the mget query for the ids: ")
		log.Println(ids)
		mgClient := client.MultiGet()
		for _, id := range ids.([]interface{}) {
			mgClient.Add(elastic.NewMultiGetItem().Index(indexName).Id(id.(string)))
		}
		res, err := mgClient.Do(_ctx)
		if err == nil && len(res.Docs) != 0 {
			byt, _ := json.Marshal(res.Docs)
			json.Unmarshal(byt, &data)
		}
	}
	return data, err
}
