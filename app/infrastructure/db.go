package infrastructure

import (
	"log"
	"sync"
	"time"

	mgo "gopkg.in/mgo.v2"
)

//DB  Infrastructure layer to incorporate all the database function
type DB struct{}

//MongoDB ...
type MongoDB struct {
	MongoDatabase *mgo.Database
}

var _initMongoCtx sync.Once
var _mongoInstance *MongoDB
var _session *mgo.Session

//Database configuration constants
const (
	Debug   = false
	LocalDB = "test1"
)

//GetMongo ...
func (ds *DB) GetMongo() *mgo.Database {
	_initMongoCtx.Do(func() {
		log.Println("Connecting to Mongo...")
		var err error
		if !Debug {
			var _dbConfig = new(Configuration)
			_config := _dbConfig.GetConfiguration()
			info := &mgo.DialInfo{
				Addrs:    []string{_config.MongoDBHost},
				Timeout:  60 * time.Second,
				Database: _config.MongoDBDatabase,
				Username: _config.MongoDBUsername,
				Password: _config.MongoDBPassword,
			}
			_session, err = mgo.DialWithInfo(info)
			_session.SetMode(mgo.Monotonic, true)
			_session.SetPoolLimit(10)
			_mongoInstance = &MongoDB{MongoDatabase: _session.DB(_config.MongoDBDatabase)}
		} else {
			_session, err = mgo.Dial("mongodb://localhost:27017")
			_mongoInstance = &MongoDB{MongoDatabase: _session.DB(LocalDB)}
		}

		if err != nil {
			log.Println("Failed to connect to Mongo...")
		}

		log.Println("Connected to Mongo...")
	})
	if !Debug {
		_session.Refresh()
	}

	return _mongoInstance.MongoDatabase
}
