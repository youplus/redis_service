package app

import (
	"fmt"
	"math/rand"
	"time"

	"./controllers"
	"./tasks"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/jasonlvhit/gocron"
)

//App has router init functionalities
type App struct{}

var pushService = tasks.PushService{}
var widgetController = controllers.Widget{}

//Init initializes the app with predefined configuration
func (app *App) Init() {
	rand.Seed(time.Now().UnixNano())
	gin.SetMode(gin.ReleaseMode)
	routes := gin.Default()
	routes.Use(gzip.Gzip(gzip.DefaultCompression))
	routes.Use(gin.ErrorLogger())
	routes.Use(gin.Recovery())

	v1 := routes.Group("v1")
	{
		v1.POST("/push_widget_data", PushWidgetData)
	}
	routes.StaticFile("/favicon.ico", "")
	routes.Run(":8004")
}

//InitSheduler initializes the app with predefined configuration
//Author: Suraj
//date: 06/05/2019
func (app *App) InitSheduler() {
	//gocron.Every(1).Day().At("10:30").Do(test)
	//gocron.Every(3).Minutes().Do(test)
	gocron.Every(1).Seconds().Do(func() {
		fmt.Println("I am runnning task.", time.Now())
		//pushService.PopulateWidgetVideos()
		test()
	})
	<-gocron.Start()
}

func test() {
	fmt.Println("I am runnning task.", time.Now())
}

//PushWidgetData is module which  handles storing of widget related data to redis
//Params: widget_url
//Author: Suraj
//date: 06/05/2019
func PushWidgetData(c *gin.Context) {
	response, status, statusCode := widgetController.PushToRedis(c.PostForm("widget_url"))
	c.Header("Access-Control-Allow-Origin", "*")
	c.SecureJSON(statusCode, gin.H{
		"message": response,
		"success": status,
	})

}
